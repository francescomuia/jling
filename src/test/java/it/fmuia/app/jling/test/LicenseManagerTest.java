package it.fmuia.app.jling.test;

import it.fmuia.apps.jling.ILicense;
import it.fmuia.apps.jling.JLing;
import it.fmuia.apps.jling.License;
import it.fmuia.apps.jling.LicenseManager;
import it.fmuia.apps.jling.LicenseType;
import it.fmuia.apps.jling.examples.LicenseProviderImpl;
import it.fmuia.apps.jling.utils.SecurityUtil;

import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;

public class LicenseManagerTest {

	@Test
	public void testWriteLicense() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		PublicKey publicKey = SecurityUtil.loadPublicKey(JLing.class
				.getResourceAsStream("/META-INF/public-johndoe.pem"));
		PrivateKey privateKey = SecurityUtil.loadPrivateKey(JLing.class
				.getResourceAsStream("/META-INF/priv-johndoe.pem"));
		LicenseManager l = new LicenseManager(publicKey, privateKey,
				new LicenseProviderImpl());
		License license = new License("123141", "asfasfaf", "afasfa",
				"afasgags", LicenseType.FULL, null, null);
		l.writeLicense(
				license,
				new File(
						"C:/Users/muia/workspace web/progetti/jling/jling/src/main/resources/META-INF/license.dat"));
	}

	@Test
	public void testReadLicense() throws Exception {
		Security.addProvider(new BouncyCastleProvider());
		PublicKey publicKey = SecurityUtil.loadPublicKey(JLing.class
				.getResourceAsStream("/META-INF/public-johndoe.pem"));
		LicenseManager l = new LicenseManager(publicKey,
				new LicenseProviderImpl());

		ILicense license = l.readLicense();
		System.out.println(license);
	}
}
