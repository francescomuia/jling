/**
 * TimeZonesSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.fmuia.app.webservices;

public interface TimeZonesSoap extends java.rmi.Remote {

    /**
     * Returns an array of all time zone names.
     */
    public java.lang.String[] getTimeZoneNames() throws java.rmi.RemoteException;

    /**
     * Returns the details of a specific time zone.
     */
    public it.fmuia.app.webservices.TimeZoneInformation getTimeZoneByName(java.lang.String timeZoneName) throws java.rmi.RemoteException;

    /**
     * Specifies whether a time zone is currently experiencing daylight
     * savings.
     */
    public boolean isDaylightTime(java.lang.String timeZoneName) throws java.rmi.RemoteException;

    /**
     * Returns the current date and time for a given time zone.
     */
    public java.util.Calendar currentDateTime(java.lang.String timeZoneName) throws java.rmi.RemoteException;

    /**
     * Converts a time interpreted as UTC to a time in a named time
     * zone
     */
    public java.util.Calendar fromUniversalTime(java.lang.String timeZoneName, java.util.Calendar utcDateTime) throws java.rmi.RemoteException;
}
