package it.fmuia.app.webservices;

import java.rmi.RemoteException;
import java.util.Calendar;

import javax.xml.rpc.ServiceException;

public class DateTimeUtility {
	private static String timezone = "Central European Standard Time";

	public static Calendar getCurrentTime() throws ServiceException, RemoteException {
		TimeZonesLocator locator = new TimeZonesLocator();
		TimeZonesSoap timeZonesSoap = locator.getTimeZonesSoap();
		return timeZonesSoap.currentDateTime(timezone);
	}
}
