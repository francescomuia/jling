/**
 * TimeZonesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.fmuia.app.webservices;

public class TimeZonesLocator extends org.apache.axis.client.Service implements it.fmuia.app.webservices.TimeZones {

/**
 * A service that returns information about time zones.  Please contact
 * me if you plan to use this service so that I can keep you informed
 * about when the service changes or when it is moved.
 */

    public TimeZonesLocator() {
    }


    public TimeZonesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TimeZonesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TimeZonesSoap
    private java.lang.String TimeZonesSoap_address = "http://markitup.com/WebServices/TimeZones.asmx";

    public java.lang.String getTimeZonesSoapAddress() {
        return TimeZonesSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TimeZonesSoapWSDDServiceName = "TimeZonesSoap";

    public java.lang.String getTimeZonesSoapWSDDServiceName() {
        return TimeZonesSoapWSDDServiceName;
    }

    public void setTimeZonesSoapWSDDServiceName(java.lang.String name) {
        TimeZonesSoapWSDDServiceName = name;
    }

    public it.fmuia.app.webservices.TimeZonesSoap getTimeZonesSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TimeZonesSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTimeZonesSoap(endpoint);
    }

    public it.fmuia.app.webservices.TimeZonesSoap getTimeZonesSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            it.fmuia.app.webservices.TimeZonesSoapStub _stub = new it.fmuia.app.webservices.TimeZonesSoapStub(portAddress, this);
            _stub.setPortName(getTimeZonesSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTimeZonesSoapEndpointAddress(java.lang.String address) {
        TimeZonesSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (it.fmuia.app.webservices.TimeZonesSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                it.fmuia.app.webservices.TimeZonesSoapStub _stub = new it.fmuia.app.webservices.TimeZonesSoapStub(new java.net.URL(TimeZonesSoap_address), this);
                _stub.setPortName(getTimeZonesSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TimeZonesSoap".equals(inputPortName)) {
            return getTimeZonesSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://MarkItUp.com/WebServices/2006/01/18/TimeZones", "TimeZones");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://MarkItUp.com/WebServices/2006/01/18/TimeZones", "TimeZonesSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TimeZonesSoap".equals(portName)) {
            setTimeZonesSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
