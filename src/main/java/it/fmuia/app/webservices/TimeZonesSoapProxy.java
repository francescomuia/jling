package it.fmuia.app.webservices;

public class TimeZonesSoapProxy implements it.fmuia.app.webservices.TimeZonesSoap {
  private String _endpoint = null;
  private it.fmuia.app.webservices.TimeZonesSoap timeZonesSoap = null;
  
  public TimeZonesSoapProxy() {
    _initTimeZonesSoapProxy();
  }
  
  public TimeZonesSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initTimeZonesSoapProxy();
  }
  
  private void _initTimeZonesSoapProxy() {
    try {
      timeZonesSoap = (new it.fmuia.app.webservices.TimeZonesLocator()).getTimeZonesSoap();
      if (timeZonesSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)timeZonesSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)timeZonesSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (timeZonesSoap != null)
      ((javax.xml.rpc.Stub)timeZonesSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.fmuia.app.webservices.TimeZonesSoap getTimeZonesSoap() {
    if (timeZonesSoap == null)
      _initTimeZonesSoapProxy();
    return timeZonesSoap;
  }
  
  public java.lang.String[] getTimeZoneNames() throws java.rmi.RemoteException{
    if (timeZonesSoap == null)
      _initTimeZonesSoapProxy();
    return timeZonesSoap.getTimeZoneNames();
  }
  
  public it.fmuia.app.webservices.TimeZoneInformation getTimeZoneByName(java.lang.String timeZoneName) throws java.rmi.RemoteException{
    if (timeZonesSoap == null)
      _initTimeZonesSoapProxy();
    return timeZonesSoap.getTimeZoneByName(timeZoneName);
  }
  
  public boolean isDaylightTime(java.lang.String timeZoneName) throws java.rmi.RemoteException{
    if (timeZonesSoap == null)
      _initTimeZonesSoapProxy();
    return timeZonesSoap.isDaylightTime(timeZoneName);
  }
  
  public java.util.Calendar currentDateTime(java.lang.String timeZoneName) throws java.rmi.RemoteException{
    if (timeZonesSoap == null)
      _initTimeZonesSoapProxy();
    return timeZonesSoap.currentDateTime(timeZoneName);
  }
  
  public java.util.Calendar fromUniversalTime(java.lang.String timeZoneName, java.util.Calendar utcDateTime) throws java.rmi.RemoteException{
    if (timeZonesSoap == null)
      _initTimeZonesSoapProxy();
    return timeZonesSoap.fromUniversalTime(timeZoneName, utcDateTime);
  }
  
  
}