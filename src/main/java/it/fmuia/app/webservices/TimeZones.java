/**
 * TimeZones.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.fmuia.app.webservices;

public interface TimeZones extends javax.xml.rpc.Service {

/**
 * A service that returns information about time zones.  Please contact
 * me if you plan to use this service so that I can keep you informed
 * about when the service changes or when it is moved.
 */
    public java.lang.String getTimeZonesSoapAddress();

    public it.fmuia.app.webservices.TimeZonesSoap getTimeZonesSoap() throws javax.xml.rpc.ServiceException;

    public it.fmuia.app.webservices.TimeZonesSoap getTimeZonesSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
