package it.fmuia.apps.jling.exception;

public class SignatureNotMatchException extends Exception {

	public SignatureNotMatchException(String message) {
		super(message);
	}

}
