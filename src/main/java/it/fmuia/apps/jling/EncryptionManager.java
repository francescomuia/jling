package it.fmuia.apps.jling;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

public class EncryptionManager {

	private PublicKey publicKey;

	private PrivateKey privateKey;

	public EncryptionManager(PublicKey publicKey, PrivateKey privateKey) {
		this.publicKey = publicKey;
		this.privateKey = privateKey;
	}

	public byte[] sign(byte[] data) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		if(this.privateKey == null)
		{
			throw new UnsupportedOperationException("Can't sign when private key is not avaiable");
		}
		Signature rsaSignature = Signature.getInstance("SHA1withRSA");
		rsaSignature.initSign(privateKey);
		rsaSignature.update(data);
		
		return rsaSignature.sign();
	}

	public boolean verify(byte[] data, byte[] sig) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		
		Signature rsaSignature = Signature.getInstance("SHA1withRSA");
		rsaSignature.initVerify(publicKey);
		
		rsaSignature.update(data);
		
		return rsaSignature.verify(sig);
	}
}
