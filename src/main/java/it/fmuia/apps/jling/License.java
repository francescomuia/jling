package it.fmuia.apps.jling;

import it.fmuia.app.webservices.DateTimeUtility;

import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;

import javax.xml.rpc.ServiceException;

public class License extends ILicense {

	private String serials;

	private String name;

	private String surname;

	private String email;

	private LicenseType type;

	private Date from;

	private Date to;

	public License() {
	}

	public License(String serials, String name, String surname, String email,
			LicenseType type, Date from, Date to) {
		this.serials = serials;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.type = type;
		this.from = from;
		this.to = to;
	}

	public String getSerials() {
		return serials;
	}

	public void setSerials(String serials) {
		this.serials = serials;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LicenseType getType() {
		return type;
	}

	public void setType(LicenseType type) {
		this.type = type;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public boolean check() {
		if (LicenseType.TRIAL.equals(type)) {
			Calendar calendar;
			try {
				calendar = DateTimeUtility.getCurrentTime();
			} catch (Exception e) {
				calendar = Calendar.getInstance();
				e.printStackTrace();
			}
			return calendar.after(this.to);
		} else {
			return true;
		}
	}

	@Override
	public String toString() {
		return "License [serials=" + serials + ", name=" + name + ", surname="
				+ surname + ", email=" + email + ", type=" + type + ", from="
				+ from + ", to=" + to + "]";
	}
}
