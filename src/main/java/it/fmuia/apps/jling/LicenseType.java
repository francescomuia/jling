package it.fmuia.apps.jling;

public enum LicenseType {
	TRIAL, FULL
}
