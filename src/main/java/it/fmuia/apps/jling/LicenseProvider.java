package it.fmuia.apps.jling;

public interface LicenseProvider {

	public byte[] getEncryptedLicense();

}
