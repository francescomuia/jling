package it.fmuia.apps.jling.utils;

import it.fmuia.apps.jling.ILicense;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.io.pem.PemReader;

import test.GenerateRSAKeys;

public class SecurityUtil {

	protected static void generateKeyPair(String publicKeyFilename,
			String privateFilename) {

		try {

			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

			// Create the public and private keys
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA",
					"BC");

			// BASE64Encoder b64 = new BASE64Encoder();

			SecureRandom random = new SecureRandom();
			generator.initialize(1024, random);

			KeyPair pair = generator.generateKeyPair();
			Key pubKey = pair.getPublic();
			Key privKey = pair.getPrivate();
			BufferedWriter out = new BufferedWriter(new FileWriter(
					publicKeyFilename));
			out.write(new String(Base64.encode(pubKey.getEncoded()), "UTF-8"));
			out.close();

			out = new BufferedWriter(new FileWriter(privateFilename));
			out.write(new String(Base64.encode(privKey.getEncoded()), "UTF-8"));
			out.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static PublicKey loadPublicKey(File publicKeyFile) throws Exception {
		return loadPublicKey(new FileInputStream(publicKeyFile));
	}

	public static PublicKey loadPublicKey(InputStream in) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		final PemReader reader = new PemReader(new InputStreamReader(in));
		final byte[] pubKey = reader.readPemObject().getContent();
		final X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(pubKey);
		reader.close();
		PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
		return publicKey;

	}

	public static PrivateKey loadPrivateKey(File privateKey) throws Exception {
		return loadPrivateKey(new FileInputStream(privateKey));
	}

	public static PrivateKey loadPrivateKey(InputStream in) throws Exception {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		final PemReader reader = new PemReader(new InputStreamReader(in));
		final byte[] pubKey = reader.readPemObject().getContent();
		
		reader.close();
		PKCS8EncodedKeySpec publicKeySpec = new PKCS8EncodedKeySpec(pubKey);
		PrivateKey publicKey = keyFactory.generatePrivate(publicKeySpec);
		return publicKey;
	}

}
