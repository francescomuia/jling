package it.fmuia.apps.jling;

import it.fmuia.apps.jling.utils.SecurityUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JLing {

	private LicenseManager licenseManager;

	private static JLing instance;

	private JLing(LicenseProvider licenseProvider, PrivateKey privateKey,
			PublicKey publicKey) {
		this.licenseManager = new LicenseManager(publicKey, privateKey,
				licenseProvider);
	}

	public static JLing getInstance(LicenseProvider provider, File publicKey)
			throws Exception {
		return getInstance(provider, new FileInputStream(publicKey));
	}

	public static JLing getInstance(LicenseProvider provider, File privateKey,
			File publicKey) throws Exception {
		return getInstance(provider, new FileInputStream(publicKey),
				new FileInputStream(privateKey));
	}

	private static JLing getInstance(LicenseProvider provider,
			InputStream publicKey) throws Exception {
		if (JLing.instance == null) {
			Security.addProvider(new BouncyCastleProvider());
			JLing.instance = new JLing(provider, null,
					SecurityUtil.loadPublicKey(publicKey));
		}
		return JLing.instance;
	}

	private static JLing getInstance(LicenseProvider provider,
			InputStream publicKey, InputStream privatekey) throws Exception {
		if (JLing.instance == null) {
			Security.addProvider(new BouncyCastleProvider());
			JLing.instance = new JLing(provider,
					SecurityUtil.loadPrivateKey(privatekey),
					SecurityUtil.loadPublicKey(publicKey));
		}
		return JLing.instance;
	}

	public void writeSignedLicense(ILicense license, File file)
			throws InvalidKeyException, NoSuchAlgorithmException,
			SignatureException, IOException {
		this.licenseManager.writeLicense(license, file);
	}

	public boolean checkLicense() throws Exception {
		ILicense license = getLicense();
		return license.check();
	}

	public ILicense getLicense() throws Exception {
		return this.licenseManager.readLicense();
	}
}
