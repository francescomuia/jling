package it.fmuia.apps.jling;

import java.io.Serializable;
import java.security.Signature;

public abstract class ILicense implements Serializable {

	private byte[] signature;

	public abstract boolean check();

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

}
